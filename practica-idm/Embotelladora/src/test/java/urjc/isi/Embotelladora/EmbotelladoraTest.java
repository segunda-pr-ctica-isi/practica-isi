import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class EmbotelladoraTest{

// Este  test  implementa  la  combinacion  de  los  bloques  A1, B1, C1 y D1 de las caracterizaciones q1, q2, q3, q4 respectivamente

	@Test
	public void test_A1B1C1D1() throws Exception{
		int result = 0;

		System.out.println("~~~ A1B1C1D1");
		result = Embotelladora.calculaBotellasPequenas(5,5,30);
		assertTrue("test_A1B1C1D1", result == 5);
	}

// Este  test  implementa  la  combinacion  de  los  bloques  A1, B1, C1 y D3 de las caracterizaciones q1, q2, q3, q4 respectivamente

	@Test (expected = IllegalArgumentException.class)
	public void test_A1B1C1D3() throws Exception{
		System.out.println("~~~ A1B1C1D3");
		Embotelladora.calculaBotellasPequenas(5,5,-30);
	}

// Este  test  implementa  la  combinacion  de  los  bloques  A2, B2, C1 y D1 de las caracterizaciones q1, q2, q3, q4 respectivamente

	@Test
	public void test_A2B2C1D1() throws Exception{
		int result = 0;

		System.out.println("~~~ A2B2C1D1");
		result = Embotelladora.calculaBotellasPequenas(10,0,10);
		assertTrue("test_A2B2C1D1", result == 10);
	}

// Este  test  implementa  la  combinacion  de  los  bloques  A2, B2, C2 y D1 de las caracterizaciones q1, q2, q3, q4 respectivamente

	@Test
	public void test_A2B2C2D1() throws Exception{
		int result = 0;

		System.out.println("~~~ A2B2C2D1");
		result = Embotelladora.calculaBotellasPequenas(30,0,20);
		assertTrue("test_A2B2C2D1", result == 20);
	}

// Este  test  implementa  la  combinacion  de  los  bloques  A2, B2, C3 y D1 de las caracterizaciones q1, q2, q3, q4 respectivamente

	@Test (expected = NoSolution.class)
	public void test_A2B2C3D1() throws Exception{
		System.out.println("~~~ A2B2C3D1");
		Embotelladora.calculaBotellasPequenas(5,0,10);
	}

// Este  test  implementa  la  combinacion  de  los  bloques  A3, B3, C2 y D1 de las caracterizaciones q1, q2, q3, q4 respectivamente

	@Test
	public void test_A3B3C2D1() throws Exception{
		int result = 0;
	
		System.out.println("~~~ A3B3C2D1");
		result = Embotelladora.calculaBotellasPequenas(0,10,40);
		assertTrue("test_A3B3C2D1", result == 0);
	}

// Estos tests implementan la combinacion de los bloques A4, B4, C4 y D3 de las características q1, q2, q3 y q4 respectivamente

//pequeñas negativo

	@Test (expected = IllegalArgumentException.class)
	public void test1_A4B4C4D3() throws Exception{
		System.out.println("~~~ A4B4C4D3 test1");
		Embotelladora.calculaBotellasPequenas(-10,10,50);
	}

//grandes negativo

	@Test (expected = IllegalArgumentException.class)
	public void test2_A4B4C4D3() throws Exception{
		System.out.println("~~~ A4B4C4D3 test2");
		Embotelladora.calculaBotellasPequenas(10,-10,50);
	}

//pequeñas, grandes y total negativos

	@Test (expected = IllegalArgumentException.class)
	public void test3_A4B4C4D3() throws Exception{
		System.out.println("~~~ A4B4C4D3 test3");
		Embotelladora.calculaBotellasPequenas(-10,-10,-50);
	}
}
