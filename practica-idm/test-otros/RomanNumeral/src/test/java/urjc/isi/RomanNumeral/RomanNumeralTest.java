package urjc.isi.RomanNumeral;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class RomanNumeralTest
{

   @Test
   public void testA1B1C1D1E1()
   {
      System.out.println ("=== testA1B1C1D1E1");
      System.out.println ("===___ Q1");
      assertTrue("XXI", RomanNumeral.convierte("XXI") == 21);      
   }
  
   //Introduce una letra que no es valida ( Cumple a2)
  
  @Test (expected = IllegalArgumentException.class)
  public void test_A2B1C1D1E1() {
        System.out.println("=== A2B1C1D1E1");
        RomanNumeral.convierte("XZ");
    }
  
  //SE REPITE L D o V  ( cumple B2)
  @Test (expected = IllegalArgumentException.class)
    public void test_A1B2C1D1E1() {
        System.out.println("=== A1B2C1D1E1");
        RomanNumeral.convierte("LL");
    }
  
  
  //SE REPITE MAS DE 3 VECES I, X, C o M (Cumple C2)
  @Test (expected = IllegalArgumentException.class)
    public void test_A1B1C2D1E1() {
        System.out.println("=== A1B1C2D1E1");
        RomanNumeral.convierte("IIII");
    }
  
  
  // Usa numero multiplo de 5 para restar(Cumple D2)
  @Test (expected = IllegalArgumentException.class)
    public void test_A1B1C1D2E1() {
        System.out.println("=== A1B1C1D2E1");
        RomanNumeral.convierte("VL");
    }
    // Usa numero multiplo de 5 para restar(Cumple D2)
  @Test (expected = IllegalArgumentException.class)
    public void test_A1B1C1D1E2() {
        System.out.println("=== A1B1C1D2E2");
        RomanNumeral.convierte("ID");
    }
  //incumple 
  @Test (expected = IllegalArgumentException.class)
    public void test_A2B2C2D2E2() {
        System.out.println("=== A2B2C2D2");
        RomanNumeral.convierte("ZVLLIIII");
    }

   
}
