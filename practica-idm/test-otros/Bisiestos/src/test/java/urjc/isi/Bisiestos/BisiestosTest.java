//
// ---> 'assert': 'must be'
//

package urjc.isi.Bisiestos;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class BisiestosTest
{
   @Before
   public void setUp ()
   {}

   @After
   public void tearDown ()
   {}

   @Test
   public void testA1B1C1 ()
   {
       System.out.println ("=== testA1B1C1");
       assertTrue("multiplo de 4:", Bisiestos.esBisiesto(400)); // ok
       assertTrue("multiplo de 100:", Bisiestos.esBisiesto(800)); // ok
       assertTrue("multiplo de 400:", Bisiestos.esBisiesto(1200)); // ok
   }

   @Test
   public void testA1B1C2 ()
   {
       System.out.println ("=== testA1B1C2");
       assertTrue("multiplo de 4:", Bisiestos.esBisiesto(400)); // ok
       assertTrue("multiplo de 100:", Bisiestos.esBisiesto(0)); // ok
       assertFalse("multiplo de 400:", Bisiestos.esBisiesto(500)); // ko
   }

   @Test
   public void testA3B3C3 ()
   {
       System.out.println ("=== testA3B3C3");
       assertFalse("múltiplo de 4:", Bisiestos.esBisiesto(-300)); // ko
       assertFalse("múltiplo de 4:", Bisiestos.esBisiesto(1)); // ko
       assertFalse("múltiplo de 4:", Bisiestos.esBisiesto(757)); // ko
   }
}

