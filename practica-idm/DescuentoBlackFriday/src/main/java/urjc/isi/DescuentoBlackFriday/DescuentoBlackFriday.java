package urjc.isi.DescuentoBlackFriday;
import java.lang.IllegalArgumentException;

public class DescuentoBlackFriday {
   // @param precioOriginal es el precio de un producto marcado en la etiqueta
   // @param porcentajeDescuento es el descuento a aplicar expresado como un porcentaje que ha de ser >= 0
   //
   // --- hay que añadir implícitamente: String fecha, para que puedan realizarse correctamente las
   // caracterizaciones y bloques
   // @param fecha
   //
   // @return el precio final teniendo en cuenta que si es black friday (29 de noviembre) se aplica
   // un descuento de porcentajeDescuento
   //
   // @throws IllegalArgumentException si precioOriginal es negativo o porcentajeDescuento es negativo

   public static double precioFinal
   (double precioOriginal, double porcentajeDescuento, String fecha) throws IllegalArgumentException{
      String str[] = fecha.split("/");
      int day = Integer.parseInt(str[0]);
      int month = Integer.parseInt(str[1]);
      double precioTot;

      precioTot = 0.0;
      if (day == 29 && month == 11 && porcentajeDescuento <= 1 && porcentajeDescuento > 0)
      {
         System.out.println ("OK BlackFriday");
         precioTot = precioOriginal - precioOriginal*porcentajeDescuento;
      }
      else
      {
         System.out.println ("KO BlackFriday");
         precioTot = precioOriginal;
      }

      return precioTot;
   }
}

